{
  config,
  pkgs,
  options,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./fonts.nix
    ./nix-config.nix
  ];

  programs.zsh.enable = true;
}
