{
  config,
  pkgs,
  options,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    #      ./fonts.nix
    ./nix-config.nix
  ];

  programs.zsh.enable = true;

  services.nix-daemon.enable = true;

  system.defaults = {
    NSGlobalDomain.AppleShowAllExtensions = true;
    NSGlobalDomain.InitialKeyRepeat = 25;
    NSGlobalDomain.KeyRepeat = 4;
    NSGlobalDomain.NSNavPanelExpandedStateForSaveMode = true;
    NSGlobalDomain.PMPrintingExpandedStateForPrint = true;
    NSGlobalDomain."com.apple.mouse.tapBehavior" = 1;
    NSGlobalDomain."com.apple.trackpad.trackpadCornerClickBehavior" = 1;
    dock.autohide = true;
    dock.mru-spaces = false;
    #    dock.show-recents = false;
    #    dock.static-only = true;
    finder.AppleShowAllExtensions = true;
    finder.FXEnableExtensionChangeWarning = false;
    #    loginwindow.GuestEnabled = false;
  };
    fonts = {
      packages = with pkgs; [
        (nerdfonts.override {fonts = ["FiraCode" "JetBrainsMono"];})
        font-awesome
        material-design-icons
        #        nur.repos.devins2518.iosevka-serif
        tenderness
        spleen
      ];
    };

}
