{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.umlet;
in {
  options.modules.umlet = {enable = mkEnableOption "umlet";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.umlet
    ];
  };
}
