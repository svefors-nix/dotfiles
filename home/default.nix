{
  inputs,
  pkgs,
  config,
  ...
}: {
  home.stateVersion = "24.05";
  imports = [
    #gui
    ./jetbrains.idea-community
    ./jetbrains.idea-ultimate
    ./vscode
    ./nvim
    ./slack.nix
    ./umlet.nix
    ./zoom-us.nix
    #       ./firefox
    #        ./foot
    #        ./eww
    #        ./dunst
    #        ./hyprland
    #        ./wofi

    # cli
    ./zsh.nix
    ./_1password-cli.nix
    ./_1password-gui.nix
    ./kitty.nix
    ./alacritty
    ./firefox.nix
    ./chromium.nix
    ./tldr.nix
    ./keyboard.nix
    ./dev
    #        ./git
    #        ./gpg
    #        ./direnv

    # system
    #        ./xdg
    #	    ./packages
  ];
  # Some sensible defaults

  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;

}
