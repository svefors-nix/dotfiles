{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.zoom-us;
in {
  options.modules.zoom-us = {enable = mkEnableOption "zoom-us";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.zoom-us
    ];
  };
}
