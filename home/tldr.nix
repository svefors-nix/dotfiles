#
# Shell
#
{
  config,
  pkgs,
  libs,
  ...
}: {
  home.packages = [
    pkgs.tldr
  ];
}
