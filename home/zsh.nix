{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.zsh;
in {
  options.modules.zsh = {enable = mkEnableOption "zsh";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.zsh
      pkgs.tldr
    ];

    programs = {
      zsh = {
        enable = true;
        syntaxHighlighting.enable = true;
		autosuggestion.enable = true;
      
#      enableSyntaxHighlighting = true;
      enableCompletion = true;
        history.size = 100000;
        #histSize = 100000;
        dotDir = ".config/zsh";
        oh-my-zsh = {
          # Extra plugins for zsh
          enable = true;
          plugins = ["git"];
        };

        #      shellInit = ''                            # Zsh theme
        #        # Spaceship
        #        source ${pkgs.spaceship-prompt}/share/zsh/site-functions/prompt_spaceship_setup
        #        autoload -U promptinit; promptinit
        ##       source $HOME/.config/shell/shell_init
        #        # Hook direnv
        ##       emulate zsh -c "$(direnv hook zsh)"
        #        # Swag
        #        pfetch                                  # Show fetch logo on terminal start
        #      '';
      };
    };
  };
}
