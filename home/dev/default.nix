{
  inputs,
  pkgs,
  config,
  ...
}: {
  home.stateVersion = "24.05";
  imports = [
    ./svefors
    ./ada-msvefors
  ];
}
