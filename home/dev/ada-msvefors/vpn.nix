{
  config,
  lib,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    openconnect
    (pkgs.writeShellScriptBin "vpn-adaptive" (builtins.readFile ./vpn-adaptive))
  ];
}
