{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.dev-ada-msvefors;
in {
  options.modules.dev-ada-msvefors = {enable = mkEnableOption "dev-ada-msvefors";};

  config = mkIf cfg.enable {
    programs.ssh.enable = true;
    programs.ssh.matchBlocks = {
      "gitlab-ada-msvefors" = {
        hostname = "gitlab.com";
        user = "ada-msvefors";
        identityFile = "~/.ssh/gitlab-ada-msvefors";
      };
    };
    home.file."dev/ada-msvefors/.gitconfig.inc" = {
      source = ./.gitconfig.inc;
    };

    programs.git = {
      enable = true;
      includes = [
        {
          path = "~/dev/ada-msvefors/.gitconfig.inc";
          condition = "gitdir:~/dev/ada-msvefors/**/";
        }
      ];

    };

    home.packages = with pkgs; [
      devbox
    ];
  };
}
