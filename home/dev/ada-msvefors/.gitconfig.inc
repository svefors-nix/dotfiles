[user]
	email = "msvefors@adaptivebiotech.com"
	name = "Mats Svefors"

[url "git@gitlab-ada-msvefors"]
  insteadOf = git@gitlab.com
