[user]
	email = "git@sweforce.com"
	name = "Mats Svefors"

[url "git@gitlab-svefors"]
  insteadOf = git@gitlab.com

[url "git@github-svefors"]
  insteadOf = git@github.com
