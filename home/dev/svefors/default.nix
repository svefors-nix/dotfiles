{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.dev-svefors;
in {
  options.modules.dev-svefors = {enable = mkEnableOption "dev-svefors";};

  config = mkIf cfg.enable {
    programs.ssh.enable = true;
    programs.ssh.matchBlocks = {
      "gitlab-svefors" = {
        hostname = "gitlab.com";
        user = "svefors";
        identityFile = "~/.ssh/gitlab-svefors";
      };
      "github-svefors" = {
        hostname = "github.com";
        user = "svefors";
        identityFile = "~/.ssh/github-svefors";
      };
    };
    home.file."dev/svefors/.gitconfig.inc" = {
      source = ./.gitconfig.inc;
    };
    programs.git = {
      enable = true;
      includes = [
        {
          path = "~/dev/svefors/.gitconfig.inc";
          condition = "gitdir:~/dev/svefors/**/";
        }
      ];
      #        userName  = "Mats Svefors";
      #        userEmail = "git-svefors@sweforce.com";
      #        extraConfig = ''
      #          [url "git@gitlab.com-svefors"]
      #            insteadOf = git@github.com
      #
      #          [url "git@github.com-svefors"]
      #            insteadOf = git@github.com
      #        '';
      #        includes = [
      #        {
      #          condition = "gitdir:~/dev/svefors/*";
      #          contents =  {
      #                        url "git@github.com-svefors"]
      #                          insteadOf = git@github.com
      #                        [url "git@gitlab.com-svefors"]
      #                          insteadOf = git@gitlab.com'';
      #          }
      #        ];
    };
  };
}
