{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules._1password-cli;
in {
  options.modules._1password-cli = {enable = mkEnableOption "_1password-cli";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs._1password
    ];
  };
}
