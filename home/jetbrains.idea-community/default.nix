{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.idea-community;
in {
  options.modules.idea-community = {enable = mkEnableOption "idea-community";};

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      jetbrains.idea-community
    ];
  };
}
