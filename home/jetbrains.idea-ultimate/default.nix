{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.idea-ultimate;
in {
  options.modules.idea-ultimate = {enable = mkEnableOption "idea-ultimate";};

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      jetbrains.idea-ultimate
    ];
  };
}
