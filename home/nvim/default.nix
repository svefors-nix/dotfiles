# modules/nvim.nix - Neo Vim
{
  lib,
  config,
  pkgs,
  ...
}:
with lib; let
  cfg = config.modules.nvim;
  # Source my theme
  jabuti-nvim = pkgs.vimUtils.buildVimPlugin {
    name = "jabuti-nvim";
    src = pkgs.fetchFromGitHub {
      owner = "jabuti-theme";
      repo = "jabuti-nvim";
      rev = "17f1b94cbf1871a89cdc264e4a8a2b3b4f7c76d2";
      sha256 = "sha256-iPjwx/rTd98LUPK1MUfqKXZhQ5NmKx/rN8RX1PIuDFA=";
    };
  };
in {
  options.modules.nvim = {enable = mkEnableOption "nvim";};
  config = mkIf cfg.enable {
    home.file.".config/nvim/settings.lua".source = ./init.lua;

    home.packages = with pkgs; [
#      rnix-lsp
      nixfmt # Nix
      sumneko-lua-language-server
      stylua # Lua
      zk
    ];

    programs.zsh = {
      initExtra = ''
        export EDITOR="nvim"
      '';

      shellAliases = {
        v = "nvim -i NONE";
        nvim = "nvim -i NONE";
      };
    };

    programs.neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
      plugins = with pkgs.vimPlugins; [
        # Syntax
        vim-nix
        vim-markdown
        editorconfig-vim

        # Quality of life
        vim-lastplace # Opens document where you left it
        auto-pairs # Print double quotes/brackets/etc
        vim-gitgutter # See uncommitted changes of file :GitGutterEnable

        # File Tree
        nerdtree # File Manager - set in extraConfig to F6

        # Customization
        wombat256-vim # Color scheme for lightline
        srcery-vim # Color scheme for text

        lightline-vim # Info bar at bottom
        #        indent-blankline-nvim # Indentation lines

        plenary-nvim
        {
          plugin = zk-nvim;
          config = "lua require('zk').setup()";
        }
        {
          plugin = jabuti-nvim;
          config = "colorscheme jabuti";
        }
        {
          plugin = impatient-nvim;
          config = "lua require('impatient')";
        }
        {
          plugin = lualine-nvim;
          config = "lua require('lualine').setup()";
        }
        {
          plugin = telescope-nvim;
          config = "lua require('telescope').setup()";
        }
        #        {
        #          plugin = indent-blankline-nvim;
        #          config = "lua require('indent_blankline').setup()";
        #        }
        {
          plugin = nvim-lspconfig;
          config = ''
            lua << EOF
            require('lspconfig').rust_analyzer.setup{}

            require('lspconfig').rnix.setup{}
            require('lspconfig').zk.setup{}
            EOF
          '';
        }
        {
          plugin = nvim-treesitter;
          config = ''
            lua << EOF
            require('nvim-treesitter.configs').setup {
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = false,
                },
            }
            EOF
          '';
        }
      ];
      extraConfig = ''
        syntax enable                             " Syntax highlighting
        colorscheme srcery                        " Color scheme text
        let g:lightline = {
          \ 'colorscheme': 'wombat',
          \ }                                     " Color scheme lightline
        highlight Comment cterm=italic gui=italic " Comments become italic
        hi Normal guibg=NONE ctermbg=NONE         " Remove background, better for personal theme
        set number                                " Set numbers
        nmap <F6> :NERDTreeToggle<CR>             " F6 opens NERDTree
        luafile ~/.config/nvim/settings.lua
      '';
    };
  };
}
