{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules.keyboard;
in {
  options.modules.keyboard = {enable = mkEnableOption "keyboard";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.qmk
      pkgs.chrysalis
    ];
  };
}
