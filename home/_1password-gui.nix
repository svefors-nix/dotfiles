{
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.modules._1password-gui;
in {
  options.modules._1password-gui = {enable = mkEnableOption "_1password-gui";};

  config = mkIf cfg.enable {
    home.packages = [
      pkgs._1password-gui
    ];
  };
}
