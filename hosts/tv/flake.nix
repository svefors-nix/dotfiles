{
  description = "TV setup";
  inputs = {
   # nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
   # home-manager.url = "github:nix-community/home-manager/release-23.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs @ {
    self,
    alejandra,
    nixpkgs,
    home-manager,
    ...
  }: {
    formatter.aarch64-linux = nixpkgs.legacyPackages.aarch64-linux.alejandra;
    nixosConfigurations.tv = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules = [
        # Modules that are used.
        ../../system/nix-config.nix
        ./configuration.nix

        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;

          home-manager.users.msvefors = {
            imports = [../../home];
            config.modules = {
              zsh.enable = true;
              kitty.enable = true;
              nvim.enable = true;
              _1password-cli.enable = true;
              slack.enable = false;
              umlet.enable = false;
              dev-svefors.enable = true;
              dev-ada-msvefors.enable = false;
              alacritty.enable = true;
              zoom-us.enable = false;
              firefox.enable = true;
              chromium.enable = true;
            };

          };
          # Optionally, use home-manager.extraSpecialArgs to pass
          # arguments to home.nix
        }
      ];
    };
  };
}
