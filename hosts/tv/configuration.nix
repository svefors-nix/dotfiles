{ config, pkgs, lib, ... }:

let
  hostname = "tv";
in {

  boot = {
    kernelPackages = pkgs.linuxKernel.packages.linux_rpi4;
    initrd.availableKernelModules = [ "xhci_pci" "usbhid" "usb_storage" ];
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };
  };

 programs.zsh.enable = true;

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };
  
  #networking.networkmanager.enable = true;
  
  networking = {
    networkmanager.enable = true;
    hostName = hostname;
    useDHCP = false;
    interfaces.end0.useDHCP = true;
    interfaces.wlan0.useDHCP = true;
  };
  environment.systemPackages = with pkgs; [ vim ];
  
  services.openssh.enable = true;

  users.users.msvefors = {
    # System User
    isNormalUser = true;
    extraGroups = ["wheel" "docker" "video" "audio" "camera" "networkmanager" "lp" "scanner" "kvm" "libvirtd" "plex" "keys"];
    shell = pkgs.zsh; # Default shell
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGkPTf0jXbVriXGjDnr99LKLme7DpLTNgYFdHy6tGiCp"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKBZJuwYEji+ToChlwgmxIfFkjgJw5AnoeAlltrOJ1hL mats@sweforce.com"
    ];
  };

  security.sudo.wheelNeedsPassword = false; # User does not need to give password when using sudo.

  i18n.defaultLocale = "en_US.UTF-8";

  services.xserver.enable = true;
  services.xserver.desktopManager.kodi.enable = true;
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "kodi";

 # This may be needed to force Lightdm into 'autologin' mode.
 # Setting an integer for the amount of time lightdm will wait
 # between attempts to try to autologin again.
  services.xserver.displayManager.lightdm.autoLogin.timeout = 3;

  # Define a user account
  users.extraUsers.kodi.isNormalUser = true;

  hardware.enableRedistributableFirmware = true;
  system.stateVersion = "23.11";
}
