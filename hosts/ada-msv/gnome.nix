{
  config,
  lib,
  pkgs,
  host,
  ...
}: {
  #hardware = {
  #    opengl = {
  #      enable = true;
  #      driSupport = true;
  ##      driSupport32Bit = true;
  #      extraPackages = with pkgs; [
  #            intel-media-driver # LIBVA_DRIVER_NAME=iHD
  #            vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
  #            vaapiVdpau
  #            libvdpau-va-gl
  #          ];
  #    };
  #  };

  boot.initrd.kernelModules = ["i915"];

  environment.variables = {
    VDPAU_DRIVER = lib.mkIf config.hardware.opengl.enable (lib.mkDefault "va_gl");
  };

  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    libvdpau-va-gl
    intel-media-driver
    vaapiVdpau
  ];
  #  nixpkgs.config.packageOverrides = pkgs: {
  #    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  #  };
  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
}
