{
  description = "Ada Thinkpad X1 Carbon Setup";
  inputs = {
   # nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
   # home-manager.url = "github:nix-community/home-manager/release-23.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs @ {
    self,
    alejandra,
    nixpkgs,
    home-manager,
    ...
  }: {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;
    nixosConfigurations.ada-msv = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        # Modules that are used.
        ../../system/nix-config.nix
        ./configuration.nix
        ./keyboard
        home-manager.nixosModules.home-manager
        {
          hardware.keyboard.qmk.enable = true;
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;


          home-manager.users.msvefors = {
            imports = [../../home];
            config.modules = {
              zsh.enable = true;
              kitty.enable = true;
              idea-ultimate.enable = true;
              vscode.enable = true;
              nvim.enable = true;
              _1password-cli.enable = true;
              _1password-gui.enable = true;
              slack.enable = true;
              umlet.enable = true;
              dev-svefors.enable = true;
              dev-ada-msvefors.enable = true;
              alacritty.enable = true;
              zoom-us.enable = false;
              firefox.enable = true;
              chromium.enable = true;
              keyboard.enable = true;
            };

          };
          # Optionally, use home-manager.extraSpecialArgs to pass
          # arguments to home.nix
        }
      ];
    };
  };
}
