# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  options,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./gnome.nix
    ./fonts.nix
  ];
nixpkgs.config.allowUnfree = true;
  programs.zsh.enable = true;

  system.stateVersion = "22.11"; # Did you read the comment?
  services.udev.enable = true;

  networking.hostName = "ada-msv";
  networking.enableIPv6  = false;
  networking.networkmanager.enable = true;
  hardware.openrazer.enable= true;
  hardware.openrazer.users = [ "msvefors" ];
  nixpkgs.config.permittedInsecurePackages = [
    #        "qtwebkit-5.212.0-alpha4"
    "openssl-1.1.1w"
  ];

  environment.systemPackages = [
    pkgs.zstd
    pkgs.age
    pkgs.gnupg
    pkgs.devenv
    #pkgs.libsForQt5.qt5.qtwebkit
    #    pkgs.wkhtmltopdf-bin
    #    pkgs.earlyoom
  ];

  time.timeZone = "America/Vancouver";

  networking.timeServers = options.networking.timeServers.default ++ ["ntp.pool.org"];

  hardware.bluetooth.enable = true;
  hardware.pulseaudio.enable = true;
  services.blueman.enable = true;

  security.sudo.wheelNeedsPassword = false; # User does not need to give password when using sudo.

  i18n.defaultLocale = "en_US.UTF-8";

  services.openssh.enable = true;

  users.users.msvefors = {
    # System User
    isNormalUser = true;
    extraGroups = ["wheel" "docker" "video" "audio" "camera" "networkmanager" "lp" "scanner" "kvm" "libvirtd" "plex" "keys" "plugdev"];
    shell = pkgs.zsh; # Default shell
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGkPTf0jXbVriXGjDnr99LKLme7DpLTNgYFdHy6tGiCp"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKBZJuwYEji+ToChlwgmxIfFkjgJw5AnoeAlltrOJ1hL mats@sweforce.com"
    ];
  };
  virtualisation.docker.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages_latest;
  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     thunderbird
  #   ];
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
}
