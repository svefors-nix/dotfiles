{ config, pkgs, options, ... }:

{
  fonts.packages = with pkgs; [
    fira-code
    fira-code-symbols
    jetbrains-mono
    roboto
    openmoji-color
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
  ];
  fonts.fontconfig = {
        hinting.autohint = true;
        defaultFonts = {
          emoji = [ "OpenMoji Color" ];
        };
    };
}
