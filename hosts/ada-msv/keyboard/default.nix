{
  pkgs,
  lib,
  config,
  services,
  ...
}:
 {
    services.udev.extraRules = builtins.readFile ./50-zsa.rules;

    environment.systemPackages = [
              pkgs.keymapp
#              pkgs.gtk3
#              pkgs.libusb1
#              pkgs.wally-cli
#              pkgs.haskellPackages.webkit2gtk3-javascriptcore
    ];
}
