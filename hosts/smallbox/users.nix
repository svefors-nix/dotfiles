{ config, pkgs,  ... }:
{
programs.zsh.enable = true;
  users.users.msvefors = {                   # System User
    isNormalUser = true;
    extraGroups = [ "minecraft" "wheel" "video" "audio" "camera" "networkmanager" "lp" "scanner" "kvm" "libvirtd" "plex" "keys" ];
    shell = pkgs.zsh;                       # Default shell
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGkPTf0jXbVriXGjDnr99LKLme7DpLTNgYFdHy6tGiCp"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKBZJuwYEji+ToChlwgmxIfFkjgJw5AnoeAlltrOJ1hL mats@sweforce.com"
    ];
  };


}
