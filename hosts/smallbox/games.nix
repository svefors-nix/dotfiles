{ config, pkgs, ... }:

{
  programs.java.enable = true;
  programs.steam = {
    enable = false;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    #package = pkgs.steam.override { withJava = true; };
  };
  environment.systemPackages = [
      pkgs.prismlauncher
      pkgs.openal
  ];

}
