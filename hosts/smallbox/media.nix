{ config, pkgs, ... }:

{

  services.samba-wsdd.enable = true; # make shares visible for windows 10 clients
networking.firewall.allowedTCPPorts = [
  30031
  20011
  5357 # wsdd
   445
   139
   19132
   25565
 ];
networking.firewall.allowedUDPPorts = [
  #30031
  3702 # wsdd
  137
  138
  19132
  25565
];



services.samba = {
  enable = false;
  securityType = "user";
    
  shares = {
    public = {
      path = "/mnt/shares/public";
      browseable = "yes";
      "read only" = "no";
      "guest ok" = "yes";
      "create mask" = "0777";
      "directory mask" = "0777";

    };
  };
 };

  environment.systemPackages = [
 #     pkgs.uxplay
    pkgs.pciutils
  ];
  services.jellyfin.enable = false;
  # Skipping this as I am disabling the firewall instead
  services.jellyfin.openFirewall= true;
  # mDNS
  #
  # This part may be optional for your needs, but I find it makes browsing in Dolphin easier,
  # and it makes connecting from a local Mac possible.
  services.avahi = {
    enable = true;
    nssmdns = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      hinfo = true;
      userServices = true;
      workstation = true;
    };
    extraServiceFiles = {
      smb = ''
        <?xml version="1.0" standalone='no'?><!--*-nxml-*-->
        <!DOCTYPE service-group SYSTEM "avahi-service.dtd">
        <service-group>
          <name replace-wildcards="yes">%h</name>
          <service>
            <type>_smb._tcp</type>
            <port>445</port>
          </service>
        </service-group>
      '';
    };
  };

  #services.avahi = {
  #  nssmdns = true;
  #  enable = true;
  #  publish = {
  #    enable = true;
  #    userServices = true;
  #    domain = true;
  #  };
  #};
  networking.firewall.enable = true;
  networking.firewall.allowPing = true;
  services.samba.openFirewall = true;

}
