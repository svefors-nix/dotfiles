{ pkgs, lib, inputs, ...} :
{
  imports = [ inputs.nix-minecraft.nixosModules.minecraft-servers ];
  nixpkgs.overlays = [ inputs.nix-minecraft.overlay ];

  services.minecraft-servers = {
    enable = true;
    eula = true;
    user = "msvefors";
    servers = {
      bedrock_vj_can = {
        package = pkgs.fabricServers.fabric-1_21_1;
        enable = true;
        serverProperties = {
           server-port = 25565;
           whitelist = false; #true;
           enable-rcon = true;
           "rcon.password" = "1234";
         };
        whitelist = {
          veg542 = "98bac354-e8c2-49cc-92ed-8d74af7d8898";
          CasualCord93177 = "6c3f05ee-3ced-4492-b4ab-9f0fbc811672";
        };
        symlinks = {
        # "mods" = ./mods;
        "mods" = pkgs.linkFarmFromDrvs "mods" (builtins.attrValues {
            Floodgate = pkgs.fetchurl {
              url = "https://cdn.modrinth.com/data/bWrNNfkb/versions/wPa1pHZJ/Floodgate-Fabric-2.2.4-b36.jar";
              sha256 = "sha256-ifzWrdZ4KJoQpFspdhmOQ+FJtwVMaGtfy4XQOcewV0Y=";
            };
            Geyser = pkgs.fetchurl { 
              url = "https://cdn.modrinth.com/data/wKkoqHrH/versions/9l1uUfeS/geyser-fabric-Geyser-Fabric-2.4.4-b694.jar";
              sha256 = "sha256-8W0V0HfYX0hyMvTUIzUo0414RwnaRfG7Qv80A6oavMA=";
            };
            Fabric = pkgs.fetchurl {
              url = "https://cdn.modrinth.com/data/P7dR8mSH/versions/iFnYBUfS/fabric-api-0.106.0%2B1.21.1.jar";
              sha256 = "sha256-sUONHcxgqL+bZv17oWWaJCSiUZxwh/pYrQ6+l9edWKg=";
            };
            JourneyMap = pkgs.fetchurl {
              url = "https://cdn.modrinth.com/data/lfHFW1mp/versions/CCrivBGb/journeymap-fabric-1.21.1-6.0.0-beta.28.jar";
              sha256 = "sha256-N2jWCP/gzvw2dSTA8bR5Q0JMrww88rmUCZB6QV4zS94=";
            };
#            WorldEdit = pkgs.fetchurl {
#              url = "https://cdn.modrinth.com/data/1u6JkXh5/versions/TLkb1TuS/worldedit-mod-7.3.7.jar";
#              sha256 = "sha256-Yse+iJNsXIGAxa2aB1BhMks3oTDJlKwdZnbmUzCfp2g=";
#            };
          });
        };
      };
      daddys = {
              package = pkgs.fabricServers.fabric-1_21_1;
              enable = true;
              serverProperties = {
                 server-port = 25566;
                 whitelist = false; #true;
                 enable-rcon = true;
                 "rcon.password" = "1234";
                 "rcon.port" = "25576";
               };
              whitelist = {
                veg542 = "98bac354-e8c2-49cc-92ed-8d74af7d8898";
                CasualCord93177 = "6c3f05ee-3ced-4492-b4ab-9f0fbc811672";
              };
              symlinks = {
              # "mods" = ./mods;
              "mods" = pkgs.linkFarmFromDrvs "mods" (builtins.attrValues {
                  Floodgate = pkgs.fetchurl {
                    url = "https://cdn.modrinth.com/data/bWrNNfkb/versions/wPa1pHZJ/Floodgate-Fabric-2.2.4-b36.jar";
                    sha256 = "sha256-ifzWrdZ4KJoQpFspdhmOQ+FJtwVMaGtfy4XQOcewV0Y=";
                  };
                  Geyser = pkgs.fetchurl {
                    url = "https://cdn.modrinth.com/data/wKkoqHrH/versions/9l1uUfeS/geyser-fabric-Geyser-Fabric-2.4.4-b694.jar";
                    sha256 = "sha256-8W0V0HfYX0hyMvTUIzUo0414RwnaRfG7Qv80A6oavMA=";
                  };
                  Fabric = pkgs.fetchurl {
                    url = "https://cdn.modrinth.com/data/P7dR8mSH/versions/iFnYBUfS/fabric-api-0.106.0%2B1.21.1.jar";
                    sha256 = "sha256-sUONHcxgqL+bZv17oWWaJCSiUZxwh/pYrQ6+l9edWKg=";
                  };
                  JourneyMap = pkgs.fetchurl {
                    url = "https://cdn.modrinth.com/data/lfHFW1mp/versions/CCrivBGb/journeymap-fabric-1.21.1-6.0.0-beta.28.jar";
                    sha256 = "sha256-N2jWCP/gzvw2dSTA8bR5Q0JMrww88rmUCZB6QV4zS94=";
                  };
      #            WorldEdit = pkgs.fetchurl {
      #              url = "https://cdn.modrinth.com/data/1u6JkXh5/versions/TLkb1TuS/worldedit-mod-7.3.7.jar";
      #              sha256 = "sha256-Yse+iJNsXIGAxa2aB1BhMks3oTDJlKwdZnbmUzCfp2g=";
      #            };
                });
              };
            };
    };
  };
  networking.firewall.allowedTCPPorts = [
    25565
    25566

  ];
  networking.firewall.allowedUDPPorts = [
    25565
    25566
  ];

}
