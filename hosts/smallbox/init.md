# 0 day setup
After booting the minimal usb stick.

might be good to look at how to enable wifi: https://nixos.org/manual/nixos/stable/index.html#sec-installation


## Drives 

We are using UEFI for this machine.

### Partition

parted /dev/nvme0n1 -- mklabel gpt

parted /dev/nvme0n1 -- mkpart primary 512MiB 99%

parted /dev/nvme0n1 -- mkpart primary linux-swap 99% 100%

parted /dev/nvme0n1 -- mkpart ESP fat32 1MiB 512MiB

parted /dev/nvme0n1 -- set 3 esp on

### label the partitions

mkfs.ext4 -L nixos /dev/nvme0n1p1

mkfs.ext4 -L swap /dev/nvme0n1p2

mkfs.fat -F 32 -n boot /dev/nvme0n1p3

### mount the partitions

mount /dev/disk/by-label/nixos /mnt

mkdir -p /mnt/boot

mount /dev/disk/by-label/boot /mnt/boot

## Generate default configuration
nixos-generate-config --root /mnt

cat /mnt/etc/nixos/hardware-configuration.nix

update to use the by-label instead

Scan for WiFi networks in a new location:

nmcli device wifi rescan

nmcli device wifi list

nmcli device wifi connect WiFiName --ask


