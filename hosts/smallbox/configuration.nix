# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =[
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./users.nix
#      ./k3s/k3s.nix
      ./media.nix
      ./games.nix
#      ../../../sops
  ];
  services.openssh.enable = true;
  systemd.targets.sleep.enable = false;
  systemd.targets.suspend.enable = false;
  systemd.targets.hibernate.enable = false;
  systemd.targets.hybrid-sleep.enable = false;

  security.sudo.wheelNeedsPassword = false; # User does not need to give password when using sudo.
#    nixpkgs.buildPlatform.system = "x86_64-linux";
 #   nixpkgs.hostPlatform.system = "aarch64-linux";


#  hardware = {
#    opengl = {
#      enable = true;
#      driSupport = true;
#    };
#  };

#    # Allow swaylock to unlock the computer for us
#  security.pam.services.swaylock = {
#    text = "auth include login";
#  };

#  xdg = {
#    portal = {
#      enable = true;
#      extraPortals = with pkgs; [
#        xdg-desktop-portal-wlr
#        xdg-desktop-portal-gtk
#      ];
#    };
#  };
#
#  services.xserver.enable = true;
#  services.xserver.displayManager.sddm.enable = true;
#  services.xserver.desktopManager.plasma5.enable = true;

#  services.xserver.enable = true;
#  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
#  services.xserver.desktopManager.plasma5.bigscreen.enable = true;
#  services.xserver.desktopManager.plasma5.useQtScaling = true;

# services.xserver.desktopManager.plasma5.excludePackages = with pkgs.libsForQt5; [
    #konsole
  #];
  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
        systemd-boot = {
          enable = true;
       	  configurationLimit = 5;
        };
  	efi = {
	  canTouchEfiVariables = true;
	  efiSysMountPoint = "/boot";
	};
        timeout = 5;
    };
    kernelPackages = pkgs.linuxPackages_latest;

  };
  networking = {
    hostName = "smallbox"; # Define your hostname.
  #    wireless.enable = true;

    networkmanager.enable = true;
    interfaces = {
      wlp0s20f3 = {
        useDHCP = true;
      };
    };
  };

  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot

  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";


  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;




  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     thunderbird
  #   ];
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
#  system.stateVersion = "22.11"; # Did you read the comment?
#  nix = {
#    package = pkgs.nixFlakes;
#    extraOptions = "experimental-features = nix-command flakes";
#  };
#  nixpkgs.config.allowUnfree = true;
#
#  security.sudo.wheelNeedsPassword = false; # User does not need to give password when using sudo.
}

