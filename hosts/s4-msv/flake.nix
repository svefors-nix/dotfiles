{
  description = "S4 Macbook Pro Darwin setup";
  inputs = {
#        nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
#        home-manager.url = "github:nix-community/home-manager/release-23.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    # nix will normally use the nixpkgs defined in home-managers inputs, we only want one copy of nixpkgs though
    darwin.url = "github:lnl7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs"; # ...
    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs @ {
    self,
    alejandra,
    nixpkgs,
    home-manager,
    darwin,
    ...
  }: let
    vars = {
      # Variables Used In Flake
      user = "matssvefors";
      #      location = "$HOME/.setup";
      #      terminal = "alacritty";
      #      editor = "nvim";
    };
  in {
    formatter.x86_64-darwin = nixpkgs.legacyPackages.x86_64-darwin.alejandra;

    darwinConfigurations.s4-msv = darwin.lib.darwinSystem {
      system = "x86_64-darwin";
      modules = [
        ../../system/darwin.nix
        ./configuration.nix
        home-manager.darwinModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;

          home-manager.users.matssvefors = {
            imports = [../../home];
            config.modules = {
              zsh.enable = true;
              kitty.enable = true;
              idea-community.enable = true;
              idea-ultimate.enable = false;
              vscode.enable = true;
              nvim.enable = true;
              _1password-cli.enable = true;
              slack.enable = true;
              umlet.enable = true;
              dev-svefors.enable = true;
              dev-ada-msvefors.enable = true;
              alacritty.enable = true;
              zoom-us.enable = false;
            };
          };
          # Optionally, use home-manager.extraSpecialArgs to pass
          # arguments to home.nix
        }
      ];
    };
  };
}
