{
  config,
  lib,
  pkgs,
  ...
}: {
  networking = {
    computerName = "s4-msv"; # Host name
    hostName = "s4-msv";
  };

  system.stateVersion = 5;

  homebrew = {
    # Declare Homebrew using Nix-Darwin
    enable = true;
    onActivation = {
      autoUpdate = true; # Auto update packages
      upgrade = true;
      cleanup = "zap"; # Uninstall not listed packages and casks
    };
    taps = [
      "homebrew/cask-fonts"
      "weaveworks/tap"
    ];
    caskArgs = {
      no_quarantine = true;
    };
    casks = [
      "1password" # this is the gui
      "chrysalis"
      "mullvadvpn"
      "firefox"
      "google-chrome"
      "minecraft"
      "font-fira-code"
      "microsoft-teams"
      "microsoft-outlook"
      "zoom"
      "balenaetcher"
      "qmk-toolbox"
      "keymapp"
      "prismlauncher"
    ];
    brews = [
      "openconnect"
      "docker"
      "colima"
    ];
  };

  users.users.matssvefors = {
    name = "matssvefors";
    home = "/Users/matssvefors";
    shell = pkgs.zsh;
  };
}
