{ config, pkgs, ... }:
{

	imports = [
	  ../../home				
	];
	config.modules = {
	  zsh.enable = true;
	  nvim.enable = true;
	  dev-svefors.enable = true;
	  dev-ada-msvefors.enable = true;
	};
}