{
  description = "2867-LAP NixOS (WSL) setup";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-wsl.url = "github:nix-community/NixOS-WSL/main";
	home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";
  };
  
  outputs = { 
	self, 
	nixpkgs, 
	nixos-wsl, 
	home-manager,
	alejandra,
	... 
	}: {
    nixosConfigurations = {
      nixos = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          nixos-wsl.nixosModules.default
          {
            system.stateVersion = "24.05";
            wsl.enable = true;
			wsl.defaultUser = "msvefors";
          }
		  home-manager.nixosModules.home-manager
			{
			  #hardware.keyboard.qmk.enable = true;
			  home-manager.useGlobalPkgs = true;
			  home-manager.useUserPackages = true;	


			  home-manager.users.msvefors = {
				imports = [
					../../home
					
				];
				config.modules = {
				  zsh.enable = true;
				  #kitty.enable = true;
				  #idea-ultimate.enable = true;
				  #vscode.enable = true;
				  nvim.enable = true;
				  #_1password-cli.enable = true;
				  #_1password-gui.enable = true;
				  #slack.enable = true;
				  #umlet.enable = true;
				  dev-svefors.enable = true;
				  dev-ada-msvefors.enable = true;
				  #alacritty.enable = true;
				  #zoom-us.enable = false;
				  #firefox.enable = true;
				  #chromium.enable = true;
				  #keyboard.enable = true;
				};

			  };
			}
        ];
      };
    };
  };
}

